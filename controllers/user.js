const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const getUser = async (req, res) => {
  const allUser = await prisma.user.findMany();
  if (allUser) {
    res.json(allUser);
  } else {
    res.status(500).json({ error: "erreur lors de la recuperation des user" });
  }
};

const postUser = async (req, res) => {
  const newUseur = await prisma.user.create({ data: req.body });
  if (newUseur) {
    res.json(newUseur);
  } else {
    res.status(500).json({ error: "erreur lors de la creation de cet user" });
  }
};

const putUser = async (req, res) => {
  const id = req.params.id;
  //   const newAge = req.body.age;
  const upDateUser = await prisma.user.updateMany({
    where: { id: parseInt(id) },
    data: req.body,
  });

  if (upDateUser) {
    res.json(upDateUser);
  } else {
    res
      .status(500)
      .json({ error: "erreur lors de la mise a jour de cet user" });
  }
};
const deleteUser = async (req, res) => {
  const id = req.params.id;

  const userdeleted = await prisma.user.delete({
    where: { id: parseInt(id) },
  });

  if (userdeleted) {
    res.json(userdeleted);
  } else {
    res
      .status(500)
      .json({ error: "erreur lors de la mise a jour de cet user" });
  }
};

module.exports = {
  getUser,
  postUser,
  putUser,
  deleteUser,
};
