const { PrismaClient } = require("@prisma/client");

const prisma = new PrismaClient();

const getCars = async (req, res) => {
  const allCars = await prisma.car.findMany({
    include: {
      client: true,
    },
  });
  if (allCars) {
    res.json(allCars);
  } else {
    res.status(500).json({ error: "erreur lors de la recuperation des user" });
  }
};

const postCar = async (req, res) => {
  const newCar = await prisma.car.create({ data: req.body });
  if (newCar) {
    res.json(newCar);
  } else {
    res.status(500).json({ error: "erreur lors de la creation de cet user" });
  }
};

module.exports = {
  getCars,
  postCar,
};
