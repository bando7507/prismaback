const express = require("express");
const app = express();
const port = 4000;
const cors = require("cors");

const user = require("./routes/user.routes");
const cars = require("./routes/car.routes");

app.use(express.json());
app.use(cors());

app.use("/api/v.01/user", user);
app.use("/api/v.01/cars", cars);

app.listen(port, console.log(`le server fonctionne au port: ${port}`));
