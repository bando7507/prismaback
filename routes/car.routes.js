const express = require("express");
const { getCars, postCar } = require("../controllers/car");
const router = express.Router();

router.route("/").get(getCars).post(postCar);

module.exports = router;
