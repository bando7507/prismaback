const express = require("express");
const {
  getUser,
  postUser,
  putUser,
  deleteUser,
} = require("../controllers/user");
const router = express.Router();

router.route("/").get(getUser).post(postUser);

router.put("/:id", putUser);
router.delete("/:id", deleteUser);

module.exports = router;
